<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath }" />
<!doctype html>
<html itemscope="" itemtype="http://schema.org/WebPage" lang="zh-CN">
<head>
<title>Google</title>
</head>
<body>
	<div class="barcode">
		<img src="${ctx}/genbc?type=code128&msg=0123456789&fmt=png" height="100px" width="300px" />
	</div>
	<div class="qrCode">
		<img src="${ctx}/qRCode?tdCode=2015131420" />
	</div>
</body>
</html>