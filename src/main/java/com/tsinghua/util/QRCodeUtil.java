package com.tsinghua.util;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QRCodeUtil {

	private static final String CHARSET = "UTF-8";
	private static final String FORMAT_NAME = "PNG";

	// 二维码尺寸
	private static final int QRCODE_SIZE = 300;

	private static final int LOGO_WIDTH = 60;
	private static final int LOGO_HEIGHT = 60;

	/**
	 * 设置二维码参数
	 * 
	 * @return
	 */
	public static Map<EncodeHintType, Object> getDecodeHintType() {
		// 用于设置QR二维码参数
		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		// 设置QR二维码排错率,可选L(7%)、M(15%)、Q(25%)、H(30%),排错率越高可存储的信息越少,但对二维码清晰度的要求越小
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		// 设置编码方式
		hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		// hints.put(EncodeHintType.MARGIN, 1);
		return hints;
	}

	/**
	 * 构建初始化二维码
	 * 
	 * @param bitMatrix
	 * @return
	 */
	public static BufferedImage initBufferedImage(BitMatrix bitMatrix) {
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
			}
		}
		return image;
	}

	/**
	 * 二维码生成
	 * 
	 * @param content
	 * @return
	 * @throws Exception
	 */
	private static BufferedImage getQR_CODE(String content) throws Exception {
		BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, getDecodeHintType());
		BufferedImage image = initBufferedImage(bitMatrix);
		return image;
	}

	/**
	 * 二维码生成带Logo
	 * 
	 * @param content
	 * @param path
	 * @param needCompress
	 * @return
	 * @throws Exception
	 */
	private static BufferedImage getQR_CODE(String content, String path,
			boolean needCompress) throws Exception {
		BufferedImage image = getQR_CODE(content);
		;
		if (path != null && path.length() != 0) {
			QRCodeUtil.addLogo(image, path, needCompress);
		}
		return image;
	}

	/**
	 * 插入Logo
	 * 
	 * @param source
	 * @param path
	 * @param needCompress
	 * @throws Exception
	 */
	private static void addLogo(BufferedImage source, String path,
			boolean needCompress) throws Exception {
		File file = new File(path);
		if (!file.exists()) {
			System.err.println(path + "该文件不存在！");
			return;
		}
		Image src = ImageIO.read(new File(path));
		int width = src.getWidth(null);
		int height = src.getHeight(null);
		if (needCompress) {
			// 压缩logo
			if (width > LOGO_WIDTH) {
				width = LOGO_WIDTH;
			}
			if (height > LOGO_HEIGHT) {
				height = LOGO_HEIGHT;
			}
			Image image = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics g = tag.getGraphics();
			// 绘制缩小后的图
			g.drawImage(image, 0, 0, null);
			g.dispose();
			src = image;
		}
		// 插入LOGO
		Graphics2D graph = source.createGraphics();
		int x = (QRCODE_SIZE - width) / 2;
		int y = (QRCODE_SIZE - height) / 2;
		graph.drawImage(src, x, y, width, height, null);
		Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
		graph.setStroke(new BasicStroke(3f));
		graph.draw(shape);
		graph.dispose();
	}

	/**
	 * 生成二维码
	 * 
	 * @param content
	 * @param path
	 * @param output
	 * @param needCompress
	 * @throws Exception
	 */
	private static void encode(String content, String path,
			OutputStream output, boolean needCompress) throws Exception {
		BufferedImage image = QRCodeUtil.getQR_CODE(content, path, needCompress);
		ImageIO.write(image, FORMAT_NAME, output);
	}

	/**
	 * 生成二维码
	 * 
	 * @param content
	 * @param path
	 * @param destPath
	 * @param needCompress
	 * @throws Exception
	 */
	private static void encode(String content, String path, String destPath,
			boolean needCompress) throws Exception {
		BufferedImage image = QRCodeUtil.getQR_CODE(content, path, needCompress);
		mkdirs(destPath);
		String file = new Random().nextInt(99999999) + ".PNG";
		ImageIO.write(image, FORMAT_NAME, new File(destPath + "/" + file));
	}

	/**
	 * 生成二维码
	 * 
	 * @param content
	 * @param destPath
	 * @throws Exception
	 */
	public static void encode(String content, String destPath) throws Exception {
		QRCodeUtil.encode(content, null, destPath, false);
	}

	/**
	 * 生成二维码
	 * 
	 * @param content
	 * @param output
	 * @throws Exception
	 */
	public static void encode(String content, OutputStream output)
			throws Exception {
		QRCodeUtil.encode(content, null, output, false);
	}

	/**
	 * 当文件夹不存在时,mkdirs会自动创建多层目录,区别于mkdir(mkdir如果父目录不存在则会抛出异常)
	 * 
	 * @param destPath
	 */
	private static void mkdirs(String destPath) {
		File file = new File(destPath);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
	}

	/**
	 * 解析二维码
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static String decode(String path) throws Exception {
		return QRCodeUtil.decode(new File(path));
	}

	/**
	 * 解析二维码
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	private static String decode(File file) throws Exception {
		if (!file.exists()) {
			return null;
		}
		BufferedImage image = ImageIO.read(file);
		if (image == null) {
			return null;
		}
		LuminanceSource source = new BufferedImageLuminanceSource(image);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();
		hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
		Result result = new MultiFormatReader().decode(bitmap, hints);
		String text = result.getText();
		return text;
	}

	public static void main(String[] args) throws Exception {
		String text = "the Time℉";
		QRCodeUtil.encode(text, null, "C:", false);
	}
}