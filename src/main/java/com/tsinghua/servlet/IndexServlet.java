package com.tsinghua.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(name = "indexServlet", urlPatterns = {"/index"})
public class IndexServlet extends HttpServlet {

	private Logger logger = LoggerFactory.getLogger(IndexServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.info("url: {}", req.getRequestURI());
		Cookie cookie = new Cookie("jsessionid", req.getSession(true).getId());
		cookie.setMaxAge(7200);//单位:秒
		resp.addCookie(cookie);
		req.getRequestDispatcher("/index.jsp").forward(req, resp);
	}

}
